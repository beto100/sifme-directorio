@extends('layouts.app')


@section('styles')
	<link href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.6/united/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.11/css/dataTables.bootstrap.min.css">
@endsection


@section('scripts')
	@parent
	<script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.11/js/dataTables.bootstrap.min.js"></script>
@endsection


@section('leftside-nav')

    @if( Auth::user()->isAdmin() )
        <li id="managers-link"><a href="{{ route('admin.managers.index') }}">Administradores</a></li>
        <li id="members-link"><a href="{{ route('admin.members.index') }}">Miembros</a></li>
    @endif
        
@endsection


@section('rightside-nav')
    <li><a href="{{ route('get.changepassword') }}"><i class="fa fa-btn fa-key"></i>Cambiar contraseña</a></li>
    @parent
@endsection