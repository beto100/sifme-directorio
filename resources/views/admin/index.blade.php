@extends('layouts.admin')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Sifme Administrator</div>

                <div class="panel-body">
                    You are in the administrator area.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
