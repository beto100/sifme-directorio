@extends('layouts.admin')

@section('content')

    @include('partials.messages')

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                    
                <div class="page-header">
                    <h1>Cambiar password</h1>
                </div>

                <br>

                <form method="post" action="{{ route('post.changepassword') }}">
                    {!! csrf_field() !!}

                    <div class="form-group {{ $errors->has('current_password') ? 'has-error' : '' }}">
                        <label for="exampleInputEmail1">Password actual</label>
                        <input type="password" class="form-control" name="current_password" placeholder="" value="{{ old('current_password') }}">
                        <span class="help-block">{{ $errors->first('current_password') }}</span>
                    </div>

                    <div class="form-group {{ $errors->has('new_password') ? 'has-error' : '' }}">
                        <label for="exampleInputPassword1">Password nuevo</label>
                        <input type="password" class="form-control" name="new_password" placeholder="">
                        <span class="help-block">{{ $errors->first('new_password') }}</span>
                    </div>

                    <div class="form-group {{ $errors->has('confirm_new_password') ? 'has-error' : '' }}">
                        <label for="exampleInputPassword1">Confirmar password nuevo</label>
                        <input type="password" class="form-control" name="confirm_new_password" placeholder="">
                        <span class="help-block">{{ $errors->first('confirm_new_password') }}</span>
                    </div>

                    <button type="submit" class="btn btn-default">Submit</button>
                </form>

            </div>
        </div>
    </div>

@endsection