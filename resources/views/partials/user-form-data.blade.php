<div class="form-group {{ $errors->has('first_name') ? 'has-error' : '' }}">
    <label for="exampleInputEmail1">Nombre(s)</label>
    <input type="text" class="form-control" id="" placeholder="" name="first_name" value="{{ old('first_name') ? old('first_name') : ( $mode == 'edit' ? $user->first_name : '' ) }}">
    <span class="help-block">{{ $errors->first('first_name') }}</span>
</div>

<div class="form-group {{ $errors->has('middle_name') ? 'has-error' : '' }}">
    <label for="exampleInputPassword1">Apellido Paterno</label>
    <input type="text" class="form-control" id="" placeholder="" name="middle_name" value="{{ old('middle_name') ? old('middle_name') : ( $mode == 'edit' ? $user->middle_name : '' ) }}">
    <span class="help-block">{{ $errors->first('middle_name') }}</span>
</div>

<div class="form-group {{ $errors->has('last_name') ? 'has-error' : '' }}">
    <label for="exampleInputEmail1">Apellido Materno</label>
    <input type="text" class="form-control" id="" placeholder="" name="last_name" value="{{ old('last_name') ? old('last_name') : ( $mode == 'edit' ? $user->last_name : '' ) }}">
    <span class="help-block">{{ $errors->first('last_name') }}</span>
</div>

<div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
    <label for="exampleInputPassword1">Email</label>
    <input type="text" class="form-control" id="" placeholder="" name="email" value="{{ old('email') ? old('email') : ( $mode == 'edit' ? $user->email : '' ) }}">
    <span class="help-block">{{ $errors->first('email') }}</span>
</div>