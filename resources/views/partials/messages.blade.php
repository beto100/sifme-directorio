@if( session('global_success') )

	<div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<strong>En hora buena!</strong> {!! session('global_success') !!}
	</div>

@endif


@if( session('global_failure') )

	<div class="alert alert-danger alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<strong>Tu petición no pudo completarse!</strong> {!! session('global_failure') !!}
	</div>

@endif