@extends('layouts.admin')

@section('scripts')
    @parent
    <script>

        $(document).ready(function() {

            $('#members-link').addClass('active');

        });

    </script>
@endsection


@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                    
                <div class="page-header">
                    <h1>{{ $mode == 'edit' ? 'Editar' : 'Crear' }} Miembro</h1>
                    @if($mode == 'edit')
                        <p class="text-info">{{ $member->user->fullname() }}</p>
                    @endif
                </div>

                <br>

                <form method="post" action="{{ $mode == 'edit' ? route('admin.members.update', [$member->id]) : route('admin.members.store') }}">

                    @if($mode == 'edit')
                        {{ method_field('PUT') }}
                    @endif

                    {!! csrf_field() !!}


                    <div class="row">
                    
                    <div class="col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">Datos Personales</div>
                        <div class="panel-body">
                            
                            @include( 'partials.user-form-data', ['user' => ($mode == 'edit') ? $member->user : null] )

                            <div class="form-group {{ $errors->has('apellidos_familia') ? 'has-error' : '' }}">
                                <label for="exampleInputEmail1">Apellidos Familia</label>
                                <input type="text" class="form-control" id="" placeholder="Primer apellido esposo y primer apellido esposa" name="apellidos_familia" value="{{ old('apellidos_familia') ? old('apellidos_familia') : ( $mode == 'edit' ? $member->apellidos_familia : '' ) }}">
                                <span class="help-block">{{ $errors->first('apellidos_familia') }}</span>
                            </div>

                            <div class="form-group {{ $errors->has('rel_familiar') ? 'has-error' : '' }}">
                                <label for="exampleInputEmail1">Relación Familiar</label>
                                <select class="form-control" name="rel_familiar">
                                    <option value=""></option>
                                    @foreach(App\Helpers\Members\FormData::$relFamiliar as $key => $value)
                                        <option value="{{ $key }}" {{ old('rel_familiar') ? (old('rel_familiar') == $key ? 'selected' : '') : $mode == 'edit' ? ($member->rel_familiar == $key ? 'selected' : '') : '' }}>{{ $value }}</option>
                                    @endforeach                                
                                </select>
                                <span class="help-block">{{ $errors->first('rel_familiar') }}</span>
                            </div>

                            <div class="form-group {{ $errors->has('sexo') ? 'has-error' : '' }}">
                                <label for="exampleInputEmail1">Sexo</label>
                                <select class="form-control" name="sexo">
                                    <option value=""></option>
                                    @foreach(App\Helpers\Members\FormData::$sexo as $key => $value)
                                        <option value="{{ $key }}" {{ old('sexo') ? (old('sexo') == $key ? 'selected' : '') : $mode == 'edit' ? ($member->sexo == $key ? 'selected' : '') : '' }}>{{ $value }}</option>
                                    @endforeach                                
                                </select>
                                <span class="help-block">{{ $errors->first('sexo') }}</span>
                            </div>

                            <div class="form-group {{ $errors->has('estado_civil') ? 'has-error' : '' }}">
                                <label for="exampleInputEmail1">Estado Civil</label>
                                <select class="form-control" name="estado_civil">
                                    <option value=""></option>
                                    @foreach(App\Helpers\Members\FormData::$estadoCivil as $key => $value)
                                        <option value="{{ $key }}" {{ old('estado_civil') ? (old('estado_civil') == $key ? 'selected' : '') : $mode == 'edit' ? ($member->estado_civil == $key ? 'selected' : '') : '' }}>{{ $value }}</option>
                                    @endforeach                                
                                </select>
                                <span class="help-block">{{ $errors->first('estado_civil') }}</span>
                            </div>

                            <div class="form-group {{ $errors->has('fecha_nacimiento') ? 'has-error' : '' }}">
                                <label for="exampleInputEmail1">Fecha de Nacimiento</label>
                                <input type="date" class="form-control" id="" placeholder="" name="fecha_nacimiento" value="{{ old('fecha_nacimiento') ? old('fecha_nacimiento') : ( $mode == 'edit' ? $member->fecha_nacimiento : '' ) }}">
                                <span class="help-block">{{ $errors->first('fecha_nacimiento') }}</span>
                            </div>

                            <div class="form-group {{ $errors->has('edad') ? 'has-error' : '' }}">
                                <label for="exampleInputEmail1">Edad</label>
                                <input type="text" class="form-control" id="" placeholder="" name="edad" value="{{ old('edad') ? old('edad') : ( $mode == 'edit' ? $member->edad : '' ) }}">
                                <span class="help-block">{{ $errors->first('edad') }}</span>
                            </div>

                            <div class="form-group {{ $errors->has('fecha_matrimonio') ? 'has-error' : '' }}">
                                <label for="exampleInputEmail1">Fecha de Matrimonio</label>
                                <input type="date" class="form-control" id="" placeholder="" name="fecha_matrimonio" value="{{ old('fecha_matrimonio') ? old('fecha_matrimonio') : ( $mode == 'edit' ? $member->fecha_matrimonio : '' ) }}">
                                <span class="help-block">{{ $errors->first('fecha_matrimonio') }}</span>
                            </div>

                            <div class="form-group {{ $errors->has('celular') ? 'has-error' : '' }}">
                                <label for="exampleInputEmail1">Celular</label>
                                <input type="text" class="form-control" id="" placeholder="" name="celular" value="{{ old('celular') ? old('celular') : ( $mode == 'edit' ? $member->celular : '' ) }}">
                                <span class="help-block">{{ $errors->first('celular') }}</span>
                            </div>

                            <div class="form-group {{ $errors->has('tipo_sangre') ? 'has-error' : '' }}">
                                <label for="exampleInputEmail1">Tipo de sangre</label>
                                <input type="text" class="form-control" id="" placeholder="" name="tipo_sangre" value="{{ old('tipo_sangre') ? old('tipo_sangre') : ( $mode == 'edit' ? $member->tipo_sangre : '' ) }}">
                                <span class="help-block">{{ $errors->first('tipo_sangre') }}</span>
                            </div>

                            <div class="form-group {{ $errors->has('donador') ? 'has-error' : '' }}">
                                <label for="exampleInputEmail1">Donador</label>: &nbsp;
                                @foreach(App\Helpers\Members\FormData::$booleanValue as $key => $value)
                                    <label class="radio-inline">
                                        <input type="radio" name="donador" value="{{ $key }}" {{ old('donador') ? (old('donador') == $key ? 'cheched' : '') : $mode == 'edit' ? ($member->donador == $key ? 'checked' : '') : '' }}> {{ $value }}
                                    </label>
                                @endforeach
                                <span class="help-block">{{ $errors->first('donador') }}</span>
                            </div>

                        </div>
                    </div>
                    </div>
                    
                    <div class="col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">Domicilio</div>
                        <div class="panel-body">
                            
                            <div class="form-group {{ $errors->has('direccion') ? 'has-error' : '' }}">
                                <label for="exampleInputEmail1">Direccion</label>
                                <input type="text" class="form-control" id="" placeholder="Calle y Numero" name="direccion" value="{{ old('direccion') ? old('direccion') : ( $mode == 'edit' ? $member->direccion : '' ) }}">
                                <span class="help-block">{{ $errors->first('direccion') }}</span>
                            </div>

                            <div class="form-group {{ $errors->has('colonia') ? 'has-error' : '' }}">
                                <label for="exampleInputEmail1">Colonia</label>
                                <input type="text" class="form-control" id="" placeholder="" name="colonia" value="{{ old('colonia') ? old('colonia') : ( $mode == 'edit' ? $member->colonia : '' ) }}">
                                <span class="help-block">{{ $errors->first('colonia') }}</span>
                            </div>

                            <div class="form-group {{ $errors->has('municipio') ? 'has-error' : '' }}">
                                <label for="exampleInputEmail1">Municipio</label>
                                <input type="text" class="form-control" id="" placeholder="" name="municipio" value="{{ old('municipio') ? old('municipio') : ( $mode == 'edit' ? $member->municipio : '' ) }}">
                                <span class="help-block">{{ $errors->first('municipio') }}</span>
                            </div>

                            <div class="form-group {{ $errors->has('codigo_postal') ? 'has-error' : '' }}">
                                <label for="exampleInputEmail1">Codigo Postal</label>
                                <input type="text" class="form-control" id="" placeholder="" name="codigo_postal" value="{{ old('codigo_postal') ? old('codigo_postal') : ( $mode == 'edit' ? $member->codigo_postal : '' ) }}">
                                <span class="help-block">{{ $errors->first('codigo_postal') }}</span>
                            </div>

                            <div class="form-group {{ $errors->has('tel_casa') ? 'has-error' : '' }}">
                                <label for="exampleInputEmail1">Tel. Casa</label>
                                <input type="text" class="form-control" id="" placeholder="" name="tel_casa" value="{{ old('tel_casa') ? old('tel_casa') : ( $mode == 'edit' ? $member->tel_casa : '' ) }}">
                                <span class="help-block">{{ $errors->first('tel_casa') }}</span>
                            </div>

                        </div>
                    </div>
                    </div>


                    
                    

                    <div class="col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">Estancia en Sifme</div>
                        <div class="panel-body">
                            
                            <div class="form-group {{ $errors->has('anio_entrada') ? 'has-error' : '' }}">
                                <label for="exampleInputEmail1">Año de Entrada</label>
                                <input type="text" class="form-control" id="" placeholder="Ej. 2004" name="anio_entrada" value="{{ old('anio_entrada') ? old('anio_entrada') : ( $mode == 'edit' ? $member->anio_entrada : '' ) }}">
                                <span class="help-block">{{ $errors->first('anio_entrada') }}</span>
                            </div>

                            <div class="form-group {{ $errors->has('antiguedad') ? 'has-error' : '' }}">
                                <label for="exampleInputEmail1">Antiguedad (en años)</label>
                                <input type="text" class="form-control" id="" placeholder="Ej. 4" name="antiguedad" value="{{ old('antiguedad') ? old('antiguedad') : ( $mode == 'edit' ? $member->antiguedad : '' ) }}">
                                <span class="help-block">{{ $errors->first('antiguedad') }}</span>
                            </div>

                            <div class="form-group {{ $errors->has('id_nextel') ? 'has-error' : '' }}">
                                <label for="exampleInputEmail1">ID Nextel</label>
                                <input type="text" class="form-control" id="" placeholder="" name="id_nextel" value="{{ old('id_nextel') ? old('id_nextel') : ( $mode == 'edit' ? $member->id_nextel : '' ) }}">
                                <span class="help-block">{{ $errors->first('id_nextel') }}</span>
                            </div>

                            <div class="form-group {{ $errors->has('nextel_numero') ? 'has-error' : '' }}">
                                <label for="exampleInputEmail1">Nextel Número</label>
                                <input type="text" class="form-control" id="" placeholder="" name="nextel_numero" value="{{ old('nextel_numero') ? old('nextel_numero') : ( $mode == 'edit' ? $member->nextel_numero : '' ) }}">
                                <span class="help-block">{{ $errors->first('nextel_numero') }}</span>
                            </div>

                            <div class="form-group {{ $errors->has('ocupacion') ? 'has-error' : '' }}">
                                <label for="exampleInputEmail1">Ocupación</label>
                                <input type="text" class="form-control" id="" placeholder="" name="ocupacion" value="{{ old('ocupacion') ? old('ocupacion') : ( $mode == 'edit' ? $member->ocupacion : '' ) }}">
                                <span class="help-block">{{ $errors->first('ocupacion') }}</span>
                            </div>

                            <div class="form-group {{ $errors->has('tel_trabajo') ? 'has-error' : '' }}">
                                <label for="exampleInputEmail1">Tel. Trabajo</label>
                                <input type="text" class="form-control" id="" placeholder="" name="tel_trabajo" value="{{ old('tel_trabajo') ? old('tel_trabajo') : ( $mode == 'edit' ? $member->tel_trabajo : '' ) }}">
                                <span class="help-block">{{ $errors->first('tel_trabajo') }}</span>
                            </div>

                            <div class="form-group {{ $errors->has('estatus_laboral') ? 'has-error' : '' }}">
                                <label for="exampleInputEmail1">Estatus Laboral</label>
                                <select class="form-control" name="estatus_laboral">
                                    <option value=""></option>
                                    @foreach(App\Helpers\Members\FormData::$estatusLaboral as $key => $value)
                                        <option value="{{ $key }}" {{ old('estatus_laboral') ? (old('estatus_laboral') == $key ? 'selected' : '') : $mode == 'edit' ? ($member->estatus_laboral == $key ? 'selected' : '') : '' }}>{{ $value }}</option>
                                    @endforeach                                
                                </select>
                                <span class="help-block">{{ $errors->first('estatus_laboral') }}</span>
                            </div>

                            <div class="form-group {{ $errors->has('forma_entrada') ? 'has-error' : '' }}">
                                <label for="exampleInputEmail1">Forma Entrada</label>
                                <select class="form-control" name="forma_entrada">
                                    <option value=""></option>
                                    @foreach(App\Helpers\Members\FormData::$formaEntrada as $key => $value)
                                        <option value="{{ $key }}" {{ old('forma_entrada') ? (old('forma_entrada') == $key ? 'selected' : '') : $mode == 'edit' ? ($member->forma_entrada == $key ? 'selected' : '') : '' }}>{{ $value }}</option>
                                    @endforeach                                
                                </select>
                                <span class="help-block">{{ $errors->first('forma_entrada') }}</span>
                            </div>

                            <div class="form-group {{ $errors->has('compromiso') ? 'has-error' : '' }}">
                                <label for="exampleInputEmail1">Compromiso</label>
                                <select class="form-control" name="compromiso">
                                    <option value=""></option>
                                    @foreach(App\Helpers\Members\FormData::$compromiso as $key => $value)
                                        <option value="{{ $key }}" {{ old('compromiso') ? (old('compromiso') == $key ? 'selected' : '') : $mode == 'edit' ? ($member->compromiso == $key ? 'selected' : '') : '' }}>{{ $value }}</option>
                                    @endforeach                                
                                </select>
                                <span class="help-block">{{ $errors->first('compromiso') }}</span>
                            </div>

                            <div class="form-group {{ $errors->has('sector') ? 'has-error' : '' }}">
                                <label for="exampleInputEmail1">Sector</label>
                                <select class="form-control" name="sector">
                                    <option value=""></option>
                                    @foreach(App\Helpers\Members\FormData::$sector as $key => $value)
                                        <option value="{{ $key }}" {{ old('sector') ? (old('sector') == $key ? 'selected' : '') : $mode == 'edit' ? ($member->sector == $key ? 'selected' : '') : '' }}>{{ $value }}</option>
                                    @endforeach                                
                                </select>
                                <span class="help-block">{{ $errors->first('sector') }}</span>
                            </div>

                            <div class="form-group {{ $errors->has('servicio') ? 'has-error' : '' }}">
                                <label for="exampleInputEmail1">Servicio</label>
                                <select class="form-control" name="servicio">
                                    <option value=""></option>
                                    @foreach(App\Helpers\Members\FormData::$servicio as $key => $value)
                                        <option value="{{ $key }}" {{ old('servicio') ? (old('servicio') == $key ? 'selected' : '') : $mode == 'edit' ? ($member->servicio == $key ? 'selected' : '') : '' }}>{{ $value }}</option>
                                    @endforeach                                
                                </select>
                                <span class="help-block">{{ $errors->first('servicio') }}</span>
                            </div>

                            <div class="form-group {{ $errors->has('anio_ensenanza') ? 'has-error' : '' }}">
                                <label for="exampleInputEmail1">Año de Enseñanza</label>
                                <input type="text" class="form-control" id="" placeholder="" name="anio_ensenanza" value="{{ old('anio_ensenanza') ? old('anio_ensenanza') : ( $mode == 'edit' ? $member->anio_ensenanza : '' ) }}">
                                <span class="help-block">{{ $errors->first('anio_ensenanza') }}</span>
                            </div>

                            <div class="form-group {{ $errors->has('cumplimiento_diezmo') ? 'has-error' : '' }}">
                                <label for="exampleInputEmail1">Cumplimiento del Diezmo</label>: &nbsp;
                                @foreach(App\Helpers\Members\FormData::$booleanValue as $key => $value)
                                    <label class="radio-inline">
                                        <input type="radio" name="cumplimiento_diezmo" value="{{ $key }}" {{ old('cumplimiento_diezmo') ? (old('cumplimiento_diezmo') == $key ? 'cheched' : '') : $mode == 'edit' ? ($member->cumplimiento_diezmo == $key ? 'checked' : '') : '' }}> {{ $value }}
                                    </label>
                                @endforeach
                                <span class="help-block">{{ $errors->first('cumplimiento_diezmo') }}</span>
                            </div>

                            <div class="form-group {{ $errors->has('estatus_comunidad') ? 'has-error' : '' }}">
                                <label for="exampleInputEmail1">Estatus Comunidad</label>
                                <select class="form-control" name="estatus_comunidad">
                                    <option value=""></option>
                                    @foreach(App\Helpers\Members\FormData::$estatusComunidad as $key => $value)
                                        <option value="{{ $key }}" {{ old('estatus_comunidad') ? (old('estatus_comunidad') == $key ? 'selected' : '') : $mode == 'edit' ? ($member->estatus_comunidad == $key ? 'selected' : '') : '' }}>{{ $value }}</option>
                                    @endforeach                                
                                </select>
                                <span class="help-block">{{ $errors->first('estatus_comunidad') }}</span>
                            </div>

                        </div>
                    </div>
                    </div>

                    </div>


                    <button type="submit" class="btn btn-primary pull-right">{{ $mode == 'edit' ? 'Actualizar' : 'Guardar' }}</button>

                </form>

            </div>
        </div>
    </div>
    
@endsection
