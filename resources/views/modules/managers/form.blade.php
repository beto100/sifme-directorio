@extends('layouts.admin')

@section('scripts')
    @parent
    <script>

        $(document).ready(function() {

            $('#managers-link').addClass('active');

        });

    </script>
@endsection


@section('content')

    <div class="container">
    	<div class="row">
    		<div class="col-md-8 col-md-offset-2">
    				
    			<div class="page-header">
    				<h1>{{ $mode == 'edit' ? 'Editar' : 'Crear' }} Administrador</h1>
                    @if($mode == 'edit')
                        <p class="text-info">{{ $manager->user->fullname() }}</p>
                    @endif
    			</div>

                <br>

                <form method="post" action="{{ $mode == 'edit' ? route('admin.managers.update', [$manager->id]) : route('admin.managers.store') }}">

                    @if($mode == 'edit')
                        {{ method_field('PUT') }}
                    @endif
                    {!! csrf_field() !!}

                    @include( 'partials.user-form-data', ['user' => ($mode == 'edit') ? $manager->user : null] )

                    <div class="form-group {{ $errors->has('roles') ? 'has-error' : '' }}">
                        <label for="exampleInputPassword1">Permisos</label>

                        <select multiple class="form-control" name="roles[]">

                            @foreach($roles as $role)

                                <option value="{{ $role->id }}" {{ old('roles') ? ( in_array($role->id, old('roles')) ? 'selected' : '' ) : ( $mode == 'edit' ? (in_array($role->id, $manager->user->role_ids) ? 'selected' : '') : '' ) }}>{{ $role->name }}</option>

                            @endforeach

                        </select>

                        <span class="help-block">{{ $errors->first('roles') }}</span>
                    </div>

                    <button type="submit" class="btn btn-primary pull-right">{{ $mode == 'edit' ? 'Actualizar' : 'Guardar' }}</button>

                </form>

    		</div>
    	</div>
    </div>
    
@endsection
