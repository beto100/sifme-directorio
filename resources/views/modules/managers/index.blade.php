@extends('layouts.admin')

@section('scripts')
    @parent
    <script>
    (function() {

        $(document).ready(function() {

            $('#example').DataTable(
            {
                ordering:false
            });


            $('#managers-link').addClass('active');


            $('#check-all').click(function() 
            {
                var boolSetting = false;

                if( $(this).prop('checked') )
                {
                    boolSetting = true;
                }

                $('tbody input[type=checkbox]').each(function(index,element) {

                    $(this).prop('checked', boolSetting);

                });

            });


            $('#delete-bulk-action').click(function() 
            {
                if( $('tbody input[type=checkbox]:checked').length <= 0 )
                {
                    alert('No has seleccionado a nadie para eliminar.');
                    return;
                }

                if( confirm('Está seguro de eliminar a el/los administrador(es)?') )
                {
                    document.forms['deleteBulkForm'].submit();
                }
            });

        });
    
    })();

    </script>
@endsection


@section('content')
    
    @include('partials.messages')

    <div class="container">
    	<div class="row">
    		<div class="col-md-10 col-md-offset-1">
    				
    			<div class="page-header">
    				<h1>Administradores</h1>
    			</div>

                <br>

                <div>
                    <a href="{{ route('admin.managers.create') }}" class="btn btn-default"><i class="fa fa-plus"></i> Crear administrador</a>
                    <button id="delete-bulk-action" class="btn btn-danger"> <i class="fa fa-trash" aria-hidden="true"></i></button>
                </div>
                <br>

                <form name="deleteBulkForm" action="{{ route('managers.bulkdestroy') }}" method="post">
                    {!! csrf_field() !!}

                    <div class="table-responsive">
                    	<table id="example" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>
                                        <div class="checkbox">
                                            <label>
                                                All
                                                <input id="check-all" type="checkbox">
                                            </label>
                                        </div>
                                    </th>
                                    <th>Nombre(s)</th>
                                    <th>Ape. Paterno</th>
                                    <th>Ape. Materno</th>
                                    <th>Email</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($managers as $manager)
                                    <tr>
                                        <td>
                                            <div class="checkbox">
                                                <input name="manager_ids[]" type="checkbox" value="{{ $manager->id }}">
                                            </div>
                                        </td>
                                        <td><a href="{{ route('admin.managers.edit', [$manager->id]) }}">{{ $manager->user->first_name }}</a></td>
                                        <td>{{ $manager->user->middle_name }}</td>
                                        <td>{{ $manager->user->last_name }}</td>
                                        <td>{{ $manager->user->email }}</td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="5">No existen administradores por el momento.</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </form>

    		</div>
    	</div>
    </div>
@endsection
