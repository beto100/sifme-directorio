<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PermissionsTableSeeder::class);
        $this->call(ManagersTableSeeder::class);
        $this->call(SuperadminsTableSeeder::class);
        $this->call(MembersTableSeeder::class);
    }
}
