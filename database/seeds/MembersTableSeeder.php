<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Member;

class MembersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$faker = Faker\Factory::create();

		factory( User::class, 100 )
			->create()
			->each(function($user) use ($faker) 
			{
				$user->givePermissionTo("de_member");
				$user->member()->save(new Member([
					'apellidos_familia'		=> $faker->lastName.' '.$faker->lastName,
					'rel_familiar' 			=> $faker->randomElement($array = array('papa','mama','hijo','hija')),
					'sexo' 					=> $faker->randomElement($array = array('masculino','femenino')),
					'estado_civil' 			=> $faker->randomElement($array = array('casado','soltero','viudo')),
					'direccion' 			=> $faker->streetName.' '.$faker->buildingNumber,
					'colonia' 				=> $faker->citySuffix,
					'municipio' 			=> $faker->city,
					'codigo_postal' 		=> $faker->postcode,
					'fecha_nacimiento' 		=> $faker->date($format = 'Y-m-d', $max = 'now'),
					'edad' 					=> $faker->numberBetween($min = 20, $max = 70),
					'anio_entrada' 			=> $faker->year($max = 'now'),
					'antiguedad' 			=> $faker->randomDigitNotNull,
					'fecha_matrimonio' 		=> $faker->date($format = 'Y-m-d', $max = 'now'),
					'tel_casa' 				=> $faker->phoneNumber,
					'celular' 				=> $faker->phoneNumber,
					'id_nextel' 			=> $faker->randomNumber($nbDigits = NULL),
					'nextel_numero' 		=> $faker->phoneNumber,
					'ocupacion' 			=> $faker->randomElement($array = array('leñador','arquitecto','programador','abogado')),
					'tel_trabajo' 			=> $faker->phoneNumber,
					'estatus_laboral' 		=> $faker->randomElement($array = array('trabajo_t_completo','trabajo_t_parcial','hogar','negocio_t_completo','negocio_t_parcial','sifme_sj','estudiante')),
					'tipo_sangre' 			=> $faker->randomElement($array = array('o+','o-')),
					'donador' 				=> rand(0,1),
					'forma_entrada' 		=> $faker->randomElement($array = array('mcu','mision_solteros', 'hijo_comunidad','mcm','grupo_fundador','otro')),
					'compromiso' 			=> $faker->randomElement($array = array('pa','inicial','camino','solemne','invitado','asociado')),
					'sector' 				=> $faker->randomElement($array = array('poniente','oriente','norte','sur')),
					'servicio' 				=> $faker->randomElement($array = array('mcm','mcu','vys','altus','rp','rm','rs','coor.','sm','ss','musica','niños','mj','servidor')),
					'anio_ensenanza' 		=> $faker->year($max = 'now'),
					'cumplimiento_diezmo'	=> rand(0,1),
					'estatus_comunidad' 	=> $faker->randomElement($array = array('excelente','bueno','regular','malo')),
				]));
			});
    }
}
