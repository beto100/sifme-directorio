<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->insert([
            'name' => 'de_super_admin',
            'description' => 'Super Administrador'
        ]);

        DB::table('permissions')->insert([
            'name' => 'de_admin',
            'description' => 'Administrador'
        ]);
        
        DB::table('permissions')->insert([
            'name' => 'de_secretary',
            'description' => 'Permisos de Secretario'
        ]);
        
        DB::table('permissions')->insert([
            'name' => 'de_member',
            'description' => 'Permisos de Miembro'
        ]);
    }
}
