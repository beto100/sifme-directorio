<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class SuperadminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory( User::class )
            ->create([
                'email'     => 'beto@gmail.com',
                'password'  => 'hola'
            ])
            ->givePermissionTo("de_super_admin");
    }
}
