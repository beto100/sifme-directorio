<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Manager;

class ManagersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		factory( User::class, 100 )
			->create()
			->each(function($user) 
			{
				$user->givePermissionTo( mt_rand(0,1) ? "de_admin" : "de_secretary" );
                $user->manager()->save(new Manager());
			});
    }
}
