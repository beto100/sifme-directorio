<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable()->unsigned();
            $table->string('apellidos_familia')->nullable();
            $table->string('rel_familiar')->nullable();
            $table->string('sexo')->nullable();
            $table->string('estado_civil')->nullable();
            $table->string('direccion')->nullable();
            $table->string('colonia')->nullable();
            $table->string('municipio')->nullable();
            $table->string('codigo_postal')->nullable();
            $table->date('fecha_nacimiento')->nullable();
            $table->integer('edad')->nullable();
            $table->integer('anio_entrada')->nullable();
            $table->integer('antiguedad')->nullable();
            $table->date('fecha_matrimonio')->nullable();
            $table->string('tel_casa')->nullable();
            $table->string('celular')->nullable();
            $table->string('id_nextel')->nullable();
            $table->string('nextel_numero')->nullable();
            $table->string('ocupacion')->nullable();
            $table->string('tel_trabajo')->nullable();
            $table->string('estatus_laboral')->nullable();
            $table->string('tipo_sangre')->nullable();
            $table->boolean('donador')->nullable();
            $table->string('forma_entrada')->nullable();
            $table->string('compromiso')->nullable();
            $table->string('sector')->nullable();
            $table->string('servicio')->nullable();
            $table->integer('anio_ensenanza')->nullable();
            $table->boolean('cumplimiento_diezmo')->nullable();
            $table->string('estatus_comunidad')->nullable();
            $table->nullableTimestamps();
            
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('members');
    }
}
