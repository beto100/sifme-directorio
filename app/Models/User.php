<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function member()
    {
        return $this->hasOne('App\Models\Member');
    }

    public function manager()
    {
        return $this->hasOne('App\Models\Manager');
    }

    public function isAdmin()
    {
        if( $this->hasPermissionTo('de_super_admin') ||  
            $this->hasPermissionTo('de_admin') ||
            $this->hasPermissionTo('de_secretary') )
        {
            return true;
        }
        
        return false;
    }

    public function fullname()
    {
        return $this->first_name . ' ' . $this->middle_name;
    }

    public function getRoleIdsAttribute()
    {
        return $this->permissions()->pluck('id')->toArray();
    }


    public function setPasswordAttribute($value)
    {   
        $this->attributes['password'] = bcrypt($value);
    }
}
