<?php namespace App\Helpers\Members;

class FormData
{
	public static $relFamiliar = [
		'papa' => 'Papá',
		'mama' => 'Mamá',
		'hijo' => 'Hijo',
		'hija' => 'Hija'
	];

	public static $sexo = [
		'masculino' => 'Masculino',
		'femenino' => 'Femenino'
	];

	public static $estadoCivil = [
		'casado' => 'Casado(a)',
		'soltero' => 'Soltero(a)',
		'viudo' => 'Viudo(a)'
	];

	public static $estatusLaboral = array(
		'trabajo_t_completo' 	=> 'Trabajo T Completo',
		'trabajo_t_parcial' 	=> 'Trabajo T Parcial',
		'hogar' 				=> 'Hogar',
		'negocio_t_completo'	=> 'Negocio T Completo',
		'negocio_t_parcial' 	=> 'Negocio T Parcial',
		'sifme_sj' 				=> 'SIFME/JS',
		'estudiante' 			=> 'Estudiante'
	);

	public static $booleanValue = array(
		'1' 	=> 'Si',
		'0' 	=> 'No'
	);

	public static $formaEntrada = array(
		'mcu' 				=> 'MCU',
		'mision_solteros'	=> 'Misión Solteros', 
		'hijo_comunidad' 	=> 'Hijo Comunidad',
		'mcm' 				=> 'MCM',
		'grupo_fundador' 	=> 'Grupo Fundador',
		'otro' 				=> 'Otro'
	);

	public static $compromiso = array(
		'pa' => 'Pa',
		'inicial' => 'Inicial',
		'camino' => 'Camino',
		'solemne' => 'Solemne',
		'invitado' => 'Invitado',
		'asociado' => 'Asociado'
	);

	public static $sector = array(
		'option1' => 'Option 1',
		'option2' => 'Option 2',
		'option3' => 'Option 3'
	);

	public static $servicio = array(
		'mcm' => 'MCM',
		'mcu' => 'MCU',
		'vys' => 'VYS',
		'altus' => 'Altus',
		'rp' => 'RP',
		'rm' => 'RM',
		'rs' => 'RS',
		'coor.' => 'Coor.',
		'sm' => 'SM',
		'ss' => 'SS',
		'musica' => 'Música',
		'niños' => 'Niños',
		'mj' => 'MJ',
		'servidor' => 'Servidor'
	);

	public static $estatusComunidad = array(
		'option1' => 'Option 1',
		'option2' => 'Option 2',
		'option3' => 'Option 3'
	);

















}