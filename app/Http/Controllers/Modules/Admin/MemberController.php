<?php

namespace App\Http\Controllers\Modules\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\MemberRepository;
use DB;

class MemberController extends Controller
{
    private $members;

    public function __construct(MemberRepository $members)
    {
        $this->members = $members;
    }

    public function bulkDestroy()
    {
        $ids = request()->input('members_ids');

        foreach($ids as $id)
        {   
            $this->members->delete($id);
        }

        return redirect()->route('admin.members.index')
                    ->with('global_success', 'El/los miembro(s) fue(ron) eliminado(s) con éxito.');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $members = $this->members->findAll();

        return view( 'modules.members.index', compact('members') );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $mode = 'create';

        return view( 'modules.members.form', compact('mode') );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = array_except(request()->all(), ['_token']);

        $manager = $this->members->store( $data );

        return redirect()->route('admin.members.index')
                        ->with('global_success', 'El miembro ha sido creado con éxito.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $member = $this->members->find($id);

        $mode = "edit";

        return view('modules.members.form', compact(
            'mode',
            'member'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = array_except(request()->all(), ['_token']);

        $manager = $this->members->update($id, $data);

        return redirect()->route('admin.members.index')
                        ->with('global_success', 'El miembro ha sido actualizado con éxito.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->members->delete($id);

        return redirect()->route('admin.members.index')
                    ->with('global_success', 'El miembro ha sido eliminado con éxito.'); 
    }
}
