<?php

namespace App\Http\Controllers\Modules\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\ManagerRepository;
use Spatie\Permission\Models\Permission;

class ManagerController extends Controller
{
    private $managers;

    public function __construct( ManagerRepository $managers)
    {
        $this->managers = $managers;
    }

    public function bulkDestroy()
    {
        $ids = request()->input('manager_ids');

        foreach($ids as $id)
        {   
            $this->managers->delete($id);
        }

        return redirect()->route('admin.managers.index')
                    ->with('global_success', 'El/los administrador(es) fue(ron) eliminado(s) con éxito.');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $managers = $this->managers->findAll();

        return view( 'modules.managers.index', compact('managers') );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $mode = 'create';

        $roles = Permission::where('name','!=','de_member')->get();

        return view('modules.managers.form', compact(
            'mode',
            'roles'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(), [
            'first_name' => 'required',
            'middle_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email|unique:users',
            'roles' => 'required'
        ]);

        $roles = request()->input('roles');

        $data = array_except(request()->all(), ['_method', '_token', 'roles']);

        list($manager, $managerPassword) = $this->managers->store($data, $roles);

        return redirect()->route('admin.managers.index')
                        ->with('global_success', "El administrador ha sido creado con éxito.<br>La constraseña de este usuario es: <b>".$managerPassword."</b>");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $manager = $this->managers->find($id);

        $mode = "edit";

        $roles = Permission::where('name','!=','de_member')->get();

        return view('modules.managers.form', compact(
            'roles',
            'mode',
            'manager'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $manager = $this->managers->find($id);

        $this->validate(request(), [
            'first_name' => 'required',
            'middle_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email|unique:users,email,'.$manager->user->id,
            'roles' => 'required'
        ]);

        $roles = request()->input('roles');

        $data = array_except(request()->all(), ['_method', '_token', 'roles']);

        $manager = $this->managers->update($id, $data, $roles);

        return redirect()->route('admin.managers.index')
                        ->with('global_success', 'El administrador ha sido actualizado con éxito.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->managers->delete($id);

        return redirect()->route('admin.managers.index')
                    ->with('global_success', 'El administrador ha sido eliminado con éxito.');
    }
}
