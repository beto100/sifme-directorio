<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Hash, Auth;

class AdministratorController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /**
     * Show the application administrator.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.index');
    }


    public function get_change_password()
    {
        return view('admin.change-password');
    }


    public function post_change_password()
    {
        $this->validate(request(), [
            'current_password' => 'required',
            'new_password' => 'required',
            'confirm_new_password' => 'required|same:new_password'
        ]);

        $currentPassword = request()->input('current_password');
        if(! Hash::check($currentPassword, Auth::user()->password) ) {
            
            return back()->with('global_failure', 'Tu password actual no es el correcto.');
        }

        // update user password
        $currentUser = Auth::user();
        $currentUser->password = bcrypt( request()->input('new_password') );
        $currentUser->save();
        return back()->with('global_success', 'Tu password ha sido cambiado con éxito.'); 
    }
}
