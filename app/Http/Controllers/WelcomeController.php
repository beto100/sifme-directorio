<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class WelcomeController extends Controller
{
	public function __construct()
	{
		$this->middleware('welcomeRedirect', ['only' => [
			'index'
		]]);
	}

	public function index()
	{
		return view('welcome');
	}
}
