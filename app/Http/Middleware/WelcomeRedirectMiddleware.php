<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class WelcomeRedirectMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if( Auth::check() )
        {
            if( Auth::user()->isAdmin() )
            {
                return redirect()->route('admin');
            }

            return redirect()->route('home');
        }

        return $next($request);
    }
}
