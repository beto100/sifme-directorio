<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => 'web'], function () {

	// Welcome routes
	Route::get('/', 'WelcomeController@index');
	

    // Auth routes
    Route::auth();
    

    // Home Routes
    Route::get('/home', 'HomeController@index')->name('home');


    // Admin routes
	Route::group(['prefix' => 'admin'], function () {

		// administration welcome
		Route::get('/', 'AdministratorController@index')->name('admin');


		// restful controllers
		Route::post('managers/bulk-destroy', 'Modules\Admin\ManagerController@bulkDestroy')->name('managers.bulkdestroy');
		Route::resource('managers', 'Modules\Admin\ManagerController');

		Route::post('members/bulk-destroy', 'Modules\Admin\MemberController@bulkDestroy')->name('members.bulkdestroy');
		Route::resource('members', 'Modules\Admin\MemberController');


		// change password route (only for admins)
		Route::get('/change-password', 'AdministratorController@get_change_password')->name('get.changepassword');
		Route::post('/change-password', 'AdministratorController@post_change_password')->name('post.changepassword');

	});

});
