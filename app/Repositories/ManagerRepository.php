<?php

namespace App\Repositories;

use App\Models\User;
use App\Models\Manager;
use Spatie\Permission\Models\Permission;

class ManagerRepository
{
    /**
     * Get all of the tasks for a given user.
     *
     * @param  User  $user
     * @return Collection
     */
    public function findAll()
    {
        return  Manager::whereHas('user.permissions', function($query) {

                    $query->where('name','!=','de_member');

                })
                ->orderBy('created_at','desc')
                ->get();
    }


    public function find($id)
    {
        return Manager::findOrFail($id);
    }


    public function delete($id)
    {
        $manager = $this->find($id);

        return User::destroy( $manager->user_id );
    }


    public function update($id, $data, $roles)
    {
        $manager = $this->find($id);

        $user = User::findOrFail( $manager->user_id );

        $updateResult = $user->update($data);

        $user->permissions()->detach();

        $user->permissions()->attach($roles);

        return $updateResult;
    }


    public function store($data, $roles)
    {
        $managerPass = str_random(15);

        $data['password'] = $managerPass;

        $user = User::create($data);

        $user->permissions()->attach($roles);

        $manager = new Manager(); 

        $user->manager()->save($manager);

        return [$manager, $managerPass];
    }

}