<?php

namespace App\Repositories;

use App\Models\Member;
use App\Models\User;

class MemberRepository
{
    /**
     * Get all of the tasks for a given user.
     *
     * @param  User  $user
     * @return Collection
     */
    public function findAll()
    {
        return  Member::whereHas('user.permissions', function($query) {

                    $query->where('name','de_member');

                })
                ->orderBy('created_at','desc')
                ->get();
    }


    public function find($id)
    {
        return Member::findOrFail($id);
    }


    public function delete($id)
    {
        $member = $this->find($id);

        return User::destroy( $member->user_id );
    }


    public function update($id, $data)
    {
        $userData = [];
        $userData['first_name'] = array_pull($data, 'first_name');
        $userData['middle_name'] = array_pull($data, 'middle_name');
        $userData['last_name'] = array_pull($data, 'last_name');
        $userData['email'] = array_pull($data, 'email');

        // find models
        $manager = $this->find($id);
        $user = User::findOrFail( $manager->user_id );

        // updates
        $user->update($userData);
        $manager->update($data);
    }


    public function store($data)
    {
        $userData = [];
        $userData['first_name'] = array_pull($data, 'first_name');
        $userData['middle_name'] = array_pull($data, 'middle_name');
        $userData['last_name'] = array_pull($data, 'last_name');
        $userData['email'] = array_pull($data, 'email');

        $user = User::create($userData);

        $user->givePermissionTo('de_member');

        $user->member()->save( new Member($data) );
    }

}